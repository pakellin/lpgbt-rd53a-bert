#!/usr/bin/env python

# Developer: Alexander Ruede (CERN/KIT-IPE), Yiannis Kazas (CERN/NCSR Demokritos)

from __future__ import division, print_function, absolute_import

import sys
from time import sleep
import argparse

try:
    from .pyDTC import *
except ValueError:
    from pyDTC import *

# Create object, specifying the IP address
# and connecting to the hardware IP endpoint
uDTC = pyDTC( "192.168.1.8" )
sleep(0.2)

#----------------------------------- USER CODE -----------------------------------#

##########################
# Hit Data Readout
##########################
def HitData_readout():
    print("----------------------------------------------")
    print("----- Starting Hit-Data Readout Sequence -----")
    print("----------------------------------------------")
    uDTC.send_fast_ecr()
    print("ECR command sent")
    sleep(0.1)
    uDTC.send_fast_bcr()
    print("BCR command sent")
    # print()
    sleep(0.1)
    uDTC.reset_fast_cmds()
    sleep(0.1)
    uDTC.ResetReadout()
    sleep(0.1)
    print("Reset Fast Commands and Readout Blocks")
    print("")
    uDTC.ext_tlu_config()
    print("tlu configured")
    sleep(0.1)
    uDTC.config_dio5()
    print("dio5 configured")
    # print()
    sleep(0.1)
    uDTC.readout_block_cnfg()
    print("Readout Block Configured")
    # print()
    sleep(0.1)
    uDTC.cnfg_fast_cmds()
    print("Fast Commands Block Configured")
    # print()
    sleep(0.1)
    uDTC.config_linear_fe()
    sleep(0.1)
    print("Chip Configured")
    print()
    print("Starting Triggering Process")
    uDTC.start_trig_process()
    sleep(0.1)
    uDTC.fast_cmd_status()
    uDTC.ReadData()
    print("----------------------------------------------")
    print("------ End of Hit-Data Readout Sequence ------")
    print("----------------------------------------------")
    print()

# Read Chip Register
def ChipReg_readout(reg_addr):
    print("----------------------------------------------")
    print("-- Read Chip Register --")
    print("Resetting Register-Readout buffers")
    uDTC.Reset_chip_buffers()
    # print(" ")
    sleep(0.1)
    print("Register-Readout fifo status")
    uDTC.Rdback_fifo_status()
    print(" ")
    sleep(0.1)
    print("Chip Register Address =", reg_addr)
    uDTC.send_read_register_cmd(reg_addr,5)
    sleep(0.1)
    uDTC.Register_readout()
    sleep(0.1)
    uDTC.Rdback_fifo_status()
    print("----------------------------------------------")
    print()

def AutoRead_readout(address_a,address_b):
    print("----------------------------------------------")
    print("-- Read Auto-Read Registers --")
    print("Auto_Read_a Chip Address  =", address_a)
    print("Auto_Read_b Chip Address  =", address_b)
    uDTC.Config_autoread(address_a,address_b)
    print()
    sleep(0.1)
    # print"Readout autoread registers"
    uDTC.autoread_readback()
    print("----------------------------------------------")
    print()

# Write RD53A Chip Registers
def Write_chipReg(reg_addr,reg_data, chip_id=15):
    # print("----------------------------------------------")
    # print("-- Write Chip Register --")
    # print("Register Address =", reg_addr)
    # print("Register Data    =", reg_data)
    uDTC.write_register(reg_addr,reg_data, chip_id)
    # print("--Slow Commands Block Status --")
    # uDTC.slow_cmd_status()
    # print("----------------------------------------------")
    # print()

# Read Aurora Link Errors
def Aurora_errors(module_address,chip_address):
    print("----------------------------------------------")
    print("-- Read Aurora Link Errors --")
    print("Module Address =", module_address)
    print("Chip Address   =", chip_address)
    uDTC.Config_aurora_error_cntr(module_address,chip_address)
    sleep(0.01)
    uDTC.Read_aurora_error_cntr()
    print("----------------------------------------------")
    print()

def mgt_clockrate():
    print("----------------------------------------------")
    print("-- lpGBT Rx/Tx Clock Rates --")
    uDTC.Read_lpgbt_clocks()
    print("----------------------------------------------")
    print()

def mgtbank_rst():
    print("----------------------------------------------")
    print("-- Reset mgt Banks --")
    uDTC.Reset_mgt_banks()
    print("----------------------------------------------")
    print()

def reverse_cmd_order(enable):
    print("-----------------------------------------------------")
    print("-- Enable reversed order of command stream nibbles --")
    uDTC.Configure_nibble_order(enable)
    print("Reversed order enable =", enable)
    print("----------------------------------------------")
    print()

def reset_ber_cntr():
    print("-----------------------------------------------------")
    print("-- Reset PRBS Error Counter --")
    uDTC.Reset_prbs_cntr()
    print("----------------------------------------------")
    print()

def sel_prbs_cntr(cntr_sel):
    print("-----------------------------------------------------")
    print("-- Select between error cntr per frame (default, 0) or per bit (1) --")
    uDTC.prbs_cntr_sel(cntr_sel)
    print("Error counter per bit = {}".format(cntr_sel))
    print("----------------------------------------------")
    print()

def sel_prbs_rx_group(rx_group_sel):
    print("-----------------------------------------------------")
    print("-- Select Rx Group for PRBS Checker --")
    uDTC.prbs_rx_group_sel(rx_group_sel)
    print("RX Group Selected = {}".format(rx_group_sel))
    print("----------------------------------------------")
    print()

def read_ber():
    print("----------------------------------------------")
    print("-- Read Bit Error Counter --")
    print(uDTC.Read_PRBS_cntr())
    print("----------------------------------------------")
    print()

def read_frame_cntr():
    print("----------------------------------------------")
    print("-- Read PRBS Frame Counter --")
    print(uDTC.Read_PRBS_Frame_cntr())
    print("----------------------------------------------")
    print("")

def enable_prbs_emulator(emulator_en):
    print("-----------------------------------------------------")
    print("-- Enable emulator for PRBS Data --")
    uDTC.prbs_emul_en(emulator_en)
    print("PRBS Emulator Enable = {}".format(emulator_en))
    print("----------------------------------------------")
    print("")

def prbs_frames_to_run(frames_to_run):
    print("-----------------------------------------------------")
    print("-- Set the amount of frames PRBS Checker should run --")
    uDTC.Set_prbs_frame_to_run(frames_to_run)
    print("----------------------------------------------")
    print("")

def start_prbs_checker():
    print("-----------------------------------------------------")
    print("-- PRBS Checker Started --")
    uDTC.Start_prbs_test()
    print("----------------------------------------------")
    print("")

def stop_prbs_checker():
    print("-----------------------------------------------------")
    print("-- PRBS Checker Stopped --")
    uDTC.Stop_prbs_test()
    print("----------------------------------------------")
    print("")

def lpgbt_loopback_test(reg_addr,reg_data):
    print("-----------------------------------------------------")
    print("-- lpGBT Slow Control - LoopBack Test --")
    uDTC.lpgbt_send_write_cmd(reg_addr,reg_data)
    sleep(0.1)
    uDTC.lpgbt_read_fifo(True)
    print("----------------------------------------------")
    print("")

def lpgbt_sc_write_reg(reg_addr,reg_data):
    print("-----------------------------------------------------")
    print("-- lpGBT Slow Control - Write Register --")
    uDTC.lpgbt_send_write_cmd(reg_addr,reg_data)
    print("----------------------------------------------")
    print("")

def lpgbt_sc_read_reg(reg_addr):
    print("-----------------------------------------------------")
    print("-- lpGBT Slow Control - Read Register --")
    uDTC.read_lpgbt_reg(reg_addr)
    print("----------------------------------------------")
    print("")

def read_lpgbt_fifo():
    print("-----------------------------------------------------")
    print("-- lpGBT Slow Control - Read Fifo --")
    uDTC.lpgbt_read_fifo(True)
    print("----------------------------------------------")
    print("")

def runBERT(testTime=0, verbose=False, frames=False):
    """ Run BERT

    Args:
        testTime (0-29): number of bits checked -> 2^(5+2*n) * 32 bits at 1.28Gbps
        frames: if True, count one error per frame instead of per bit
        verbose: print test status bits and result

    Returns: bit error rate
    """
    assert(testTime >=0 and testTime <= 29)

    nFrames = 2**(5 + 2*testTime)
    nBits = nFrames * 32

    uDTC.Set_prbs_frame_to_run(nFrames)
    # count actual bit errors
    uDTC.prbs_cntr_sel(1)
    uDTC.Start_prbs_test()

    counter = 0
    while counter <= nFrames:
        sleep(.25)
        new = uDTC.Read_PRBS_Frame_cntr()
        if new == counter:
            uDTC.Stop_prbs_test()
            raise Exception("Something went wrong: frame counter is not going up!")
        counter = new

    bertResult = uDTC.Read_PRBS_cntr()

    ber = (bertResult + 1) / nBits
    if verbose:
        print("BER: {} ({}/{})".format(ber, bertResult, nBits))

    return ber

def lpgbt_ic_reset():
    print("-----------------------------------------------------")
    print("-- Reset lpGBT-FPGA IC module  --")
    uDTC.lpgbt_rst_ic()
    print("----------------------------------------------")
    print("")

def read_lpgbt_ic_status():
    print("-----------------------------------------------------")
    print("-- Status of lpGBT-FPGA IC module  --")
    uDTC.lpgbt_ic_status()
    print("----------------------------------------------")
    print("")

def read_uplink_frame():
    print("-----------------------------------------------------")
    print("-- Read Raw Uplink Data --")
    uDTC.read_uplink()
    print("----------------------------------------------")
    print("")

def lpgbt_ic_loopback_en(enable):
    print("-----------------------------------------------------")
    print("-- Enable loopback, Send IC_tx to IC_rx --")
    uDTC.lpgbt_loopback_en(enable)
    print("lpGBT IC loopback enable =", enable)
    print("----------------------------------------------")
    print("")

def Configure_lpgbt_gtx():
    print("-----------------------------------------------------")
    uDTC.ConfigureCDCE(320)
    uDTC.SynchronizeCDCE()
    print("----------------------------------------------")
    print("")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("command", choices=["rst", "init", "read", "cfg"])
    parser.add_argument("-u", "--uplink", type=int)
    args = parser.parse_args()

    # Argument: Reset sequence
    if args.command == "rst":
        uDTC.reset_setup()
    # Argument: Initialization sequence
    elif args.command == "init":
        uDTC.init_setup()
        uDTC.enable_monitor()
    elif args.command == "read":
        if not args.uplink:
            raise Exception("Need to specify uplink group")
        sel_prbs_rx_group(args.uplink)
        read_uplink_frame()
    elif args.command == "cfg":
        Configure_lpgbt_gtx()

#!/usr/bin/env python

from __future__ import division, print_function, absolute_import

import argparse
import time
from collections import OrderedDict

from lpgbt_constants import LpgbtConstants as LPGBT
from powerSupply import PowerSupply

REGS_TO_BLOW = [
    # (address, value)
    # a list because order matters! updateEnable MUST be blown last!
    (LPGBT.CLKGCONFIG0, (0xC << LPGBT.CLKGCONFIG0_CLKGCALIBRATIONENDOFCOUNT_of) | (8 << LPGBT.CLKGCONFIG0_CLKGBIASGENCONFIG_of)),
    (LPGBT.CLKGCONFIG1, LPGBT.CLKGCONFIG1_CLKGCDRRES_bm | LPGBT.CLKGCONFIG1_CLKGVCORAILMODE_bm | (8 << LPGBT.CLKGCONFIG1_CLKGVCODAC_of)),
    (LPGBT.CLKGCDRPROPCUR, (5 << LPGBT.CLKGCDRPROPCUR_CLKGCDRPROPCURWHENLOCKED_of) | (5 << LPGBT.CLKGCDRPROPCUR_CLKGCDRPROPCUR_of)),
    (LPGBT.CLKGCDRINTCUR, (5 << LPGBT.CLKGCDRINTCUR_CLKGCDRINTCURWHENLOCKED_of) | (5 << LPGBT.CLKGCDRINTCUR_CLKGCDRINTCUR_of)),
    (LPGBT.CLKGCDRFFPROPCUR, (5 << LPGBT.CLKGCDRFFPROPCUR_CLKGCDRFEEDFORWARDPROPCURWHENLOCKED_of) | (5 << LPGBT.CLKGCDRFFPROPCUR_CLKGCDRFEEDFORWARDPROPCUR_of)),
    (LPGBT.CLKGFLLINTCUR, 0xf << LPGBT.CLKGFLLINTCUR_CLKGFLLINTCUR_of),
    (LPGBT.FAMAXHEADERFOUNDCOUNT, 0xA),
    (LPGBT.FAMAXHEADERFOUNDCOUNTAFTERNF, 0xA),
    (LPGBT.FAMAXHEADERNOTFOUNDCOUNT, 0xA),
    (LPGBT.FAFAMAXSKIPCYCLECOUNTAFTERNF, 0xA),
    (LPGBT.CHIPCONFIG, LPGBT.CHIPCONFIG_HIGHSPEEDDATAININVERT_bm), # high-speed downlink inverted
    (LPGBT.POWERUP2, LPGBT.POWERUP2_PLLCONFIGDONE_bm | LPGBT.POWERUP2_UPDATEENABLE_bm) # NOT dllConfigDone
]

# PS channels powering the card and connected to VDDF2.5 pin
PSCHAN_CARD = 1 # right
PSCHAN_FUSE = 2 # left

def mapRegsToFuses(regList):
    """Map list of (register,value) to list of (fuse,value), taking into account the fact that fuses are addressed in groups of 4 registers"""
    fuseList = OrderedDict() # ordered because order matters!
    for addr,val in REGS_TO_BLOW:
        fuseAddr = 4*(addr // 4)
        offset = addr % 4
        fuseVal = fuseList.get(fuseAddr, 0)
        fuseList[fuseAddr] = fuseVal | (val << (8 * offset))
    return list(fuseList.items())

class BlowingError(Exception):
    pass

class DummyLpGBT:
    def __init__(self):
        self.dummy = True
        self.status = 0
    def write(self, addr, val):
        print("-- Writing to lpGBT register: 0x{:x} -> address 0x{:x}".format(val, addr))
        pass
    def configPLLI2C(self):
        print("-- Initialiazing PLL")
        self.status = 13
    def configDLL(self):
        print("-- Initialiazing DLL")
        self.status = 18
    def getStatus(self):
        print("-- Returning status {}".format(self.status))
        return self.status
    def read(self, addr):
        print("-- Reading lpGBT register: 0x{:x}".format(addr))
        if addr == 0x1a1: # efuse blowing status
            return 1 << 1 # success
            # return 1 # busy
            # return 1 << 3 # error
        for addrBlow,val in REGS_TO_BLOW:
            if addrBlow == addr:
                return val
        return 0

def yesNoQuestion(prompt):
    resp = raw_input(prompt + " (yes/no) ")
    return resp.lower() == "yes"

def uint32ToBytes(val):
    """ return bits [(0-7), (8-15), (16-23), (24-31)] of integer (as ints)"""
    return [((val >> 8*i) & 0xff) for i in range(4)]

def readFuses(lpGBT, addrList):
    """ Read all efuses from the address list, return as dict (address -> value) """

    results = dict()

    # read groups of 4 registers
    fuseAddrList = list(set(4*(addr // 4) for addr in addrList))

    lpGBT.write(LPGBT.FUSECONTROL, LPGBT.FUSECONTROL_FUSEREAD_bm)
    start = time.time()
    while True:
        status = lpGBT.read(LPGBT.FUSESTATUS)
        if status & LPGBT.FUSESTATUS_FUSEDATAVALID_bm:
            lpGBT.write(LPGBT.FUSECONTROL, 0)
            break
        if time.time() - start > 0.01:
            print("Could not read fuses after 0.01s - aborting!")
            lpGBT.write(LPGBT.FUSECONTROL, 0)
            return dict()

    for fuseAddr in fuseAddrList:
        lpGBT.write(LPGBT.FUSEBLOWADDL, fuseAddr & 0xff)
        lpGBT.write(LPGBT.FUSEBLOWADDH, (fuseAddr >> 8) & 0xff)
        for i in range(4):
            if fuseAddr + i in addrList:
                results[fuseAddr + i] = lpGBT.read(LPGBT.FUSEVALUESA + i)

    return results

def burnFuse(lpGBT, addr, val):
    """ Burn 'val' into fuse address 'addr' """
    if val == 0:
        print("Register {} is zero - skipping!".format(addr))
        return
    if addr % 4:
        print("Error: register address 0x{:x} is not a multiple of 4. Skipping.".format(addr))
        return

    print("Blowing 0x{:x} to address 0x{:x}...".format(val, addr))

    # prepare data to be written
    lpGBT.write(LPGBT.FUSEBLOWADDL, addr & 0xff)
    lpGBT.write(LPGBT.FUSEBLOWADDH, (addr >> 8) & 0xff)
    bytesToBlow = uint32ToBytes(val)

    for i,v in enumerate(bytesToBlow):
        lpGBT.write(LPGBT.FUSEBLOWDATAA + i, v)

    # start blowing
    lpGBT.write(LPGBT.FUSECONTROL, (12 << LPGBT.FUSECONTROL_FUSEBLOWPULSELENGTH_of) | LPGBT.FUSECONTROL_FUSEBLOW_bm)

    start = time.time()
    while True:
        status = lpGBT.read(LPGBT.FUSESTATUS)
        elapsed = time.time() - start

        if status & LPGBT.FUSESTATUS_FUSEBLOWERROR_bm:
            raise BlowingError("Error blowing fuse address 0x{:x}!".format(addr))
        if status & LPGBT.FUSESTATUS_FUSEBLOWDONE_bm:
            print("Done after {:.5f}s".format(elapsed))
            break

        if (stop - start) > 0.5:
            raise BlowingError("Error: {:.3f}s second elapsed for a single fuse and still not done - I'm going to stop the procedure!".format(elapsed))

    # deassert fuseblow
    lpGBT.write(LPGBT.FUSECONTROL, 12 << LPGBT.FUSECONTROL_FUSEBLOWPULSELENGTH_of)


def burnConfig(real):
    if real:
        if not yesNoQuestion("I'm using the real lpGBT interface - registers will be written! Are you sure?"):
            exit(0)
        from lpGBT_tools import LpGBT
        lpGBT = LpGBT("i2c")
    else:
        lpGBT = DummyLpGBT()

    if real:
        raw_input("\nI'm going to switch on the portcard on the right PS channel (2.5V) - make sure pin 5 of the DIP switch is ON and press enter to proceed.\n")
        ps = PowerSupply()
        ps.setV(PSCHAN_CARD, 2.52)
        ps.on(PSCHAN_CARD)
        time.sleep(1)
        voltage = ps.readV(PSCHAN_CARD)
        if not (voltage > 2.5 and voltage < 2.55):
            raise Exception("PS voltage is {} - something is wrong!".format(voltage))
    else:
        print("\nFake: switch on portcard.\n")

    # make sure lpGBT is in READY state - required for buse burning!
    lpGBT.configPLLI2C()
    lpGBT.configDLL()

    # unlock fuse blowing
    lpGBT.write(LPGBT.FUSEMAGIC, LPGBT.FUSE_MAGIC_NUMBER)

    # configure pulse length
    lpGBT.write(LPGBT.FUSECONTROL, 12 << LPGBT.FUSECONTROL_FUSEBLOWPULSELENGTH_of)

    # Switch on 2.5V to VDDF2V5
    if real:
        if not yesNoQuestion("\nI'm going to switch on 2.5V on the left PS channel and blow the fuses - are you sure?"):
            exit(0)
        print()
        ps.setV(PSCHAN_FUSE, 2.52)
        ps.on(PSCHAN_FUSE)
        # Wait 100ms for command to be processed and 2.5V to come up
        # FIXME find a better way to check that (reading PS voltage is too slow)
        time.sleep(0.1)
    else:
        print("\nFake: switch on VDDF2V5.\n")

    fusesToBlow = mapRegsToFuses(REGS_TO_BLOW)

    # start burning!
    start = time.time()
    try:
        for addr,val in fusesToBlow:
            burnFuse(lpGBT, addr, val)
            elapsed = time.time() - start
            if elapsed > 0.8:
                raise BlowingError("Error: {:.3f}s second elapsed and still not done - I'm going to stop the procedure!".format(elapsed))
            print()
    except Exception as e:
        print(e)
        print("Something went wrong - powering down and exiting! {:.3f}s have elapsed since the start.".format(time.time() - start))
        # RAMP DOWN
        if real:
            ps.off(PSCHAN_FUSE)
            ps.off(PSCHAN_CARD)
        exit(1)

    # we're done - ramp down 2.5V
    print("\nDone - powering down everything.")
    if real:
        ps.off(PSCHAN_FUSE)
        ps.off(PSCHAN_CARD)

    print("\nAll done after {:.5f}s!\n".format(time.time() - start))


def testConfig(real):
    raw_input("I'm going to power up the portcard to test the config! Make sure pin 5 of the DIP switch is ON and press enter to proceed.\n")
    print()
    ps = PowerSupply()
    ps.on(PSCHAN_CARD)
    time.sleep(1)

    if real:
        from lpGBT_tools import LpGBT
        lpGBT = LpGBT("i2c")
    else:
        lpGBT = DummyLpGBT()
        lpGBT.configPLLI2C()

    # PLL config should be ok!
    status = lpGBT.getStatus()
    if status < 13:
        print("\nERROR: PUSM status is {} but should be higher than 13!\n".format(status))
    else:
        print("\nPUSM status is {} - as expected!\n".format(status))

    # DLL config should work too
    try:
        lpGBT.configDLL()
    except:
        print("\nSomething went wrong - status should be 18!\n")
    print()

    # first check actual fuses
    if real:
        results = readFuses(lpGBT, [i for i,v in REGS_TO_BLOW])
        for addr,val in REGS_TO_BLOW:
            if val == results[addr]:
                print("Success reading 0x{:x} from fuse address 0x{:x}!".format(val, addr))
            else:
                print("ERROR: fuse address 0x{:x} = 0x{:x} instead of 0x{:x}".format(addr, results[addr], val))

    # then registers
    for addr,val in REGS_TO_BLOW:
        readVal = lpGBT.read(addr)
        if addr == LPGBT.POWERUP2:
            print("POWERUP2 is 0x{:x} - skipping test".format(readVal))
            continue
        if readVal == val:
            print("Success reading 0x{:x} from register address 0x{:x}!".format(val, addr))
        else:
            print("ERROR: register address 0x{:x} = 0x{:x} instead of 0x{:x}".format(addr, readVal, val))

    if real:
        raw_input("\nNow I'm going to powercycle the portcard again to test the IC channel! Press enter to proceed.\n")
        print()
        ps.off(PSCHAN_CARD)
        del lpGBT
        time.sleep(0.5)
        raw_input("\nSwitch pin 5 of the DIP switch OFF and press enter when done.\n")
        print()
        ps.on(PSCHAN_CARD)
        time.sleep(1)

        lpGBT = LpGBT("ic")
        try:
            lpGBT.configDLL()
        except:
            print("Something went wrong - status should be 18!")

    print("\nAll done, powering down portcard")
    ps.off(PSCHAN_CARD)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Burn minimal config to lpGBT efuses')
    parser.add_argument('--real', action='store_true', help='Use actual lpGBT! (by default use dummy interface)')
    parser.add_argument('what', choices=['burn', 'test'], help="What do do - burn the config or test it after it's been burned.")
    args = parser.parse_args()

    if not args.real:
        print("Using a dummy lpGBT!")
   
    if args.what == "burn":
        burnConfig(args.real)
    elif args.what == "test":
        testConfig(args.real)


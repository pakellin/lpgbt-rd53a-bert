from __future__ import division, print_function, absolute_import

import time

from lpgbt_constants import LpgbtConstants as LPGBT

class LpGBT(object):
    def __init__(self, interface):
        assert(interface.lower() in ["i2c", "ic"])
        self.interface = interface.lower()

        if self.interface == "i2c":
            import Adafruit_GPIO.FT232H as FT232H
            # Find the first FT232H device.
            ft232h = FT232H.FT232H()
            # Create an I2C device at address 0x70.
            self.i2c = FT232H.I2CDevice(ft232h, 0x70)
            self.write = self.i2c.write_lpGBT
            self.read = self.i2c.read_lpGBT
        elif self.interface == "ic":
            # FPGA interface
            import libpyDTC.fc7_daq as FC7
            self.uDTC = FC7.uDTC
            self.write = self.uDTC.lpgbt_send_write_cmd
            self.read = self.uDTC.read_lpgbt_reg

        self.dummy = False

    def getStatus(self):
        return self.read(0x1c7)

    def configPLLI2C(self):
        assert(self.interface == "i2c")

        self.i2c.write_lpGBT(0x020, 0xc8) # X
        self.i2c.write_lpGBT(0x025, 0x55) # X
        self.i2c.write_lpGBT(0x026, 0x55) # X
        self.i2c.write_lpGBT(0x036, 1 << 6) # hsin - inverted, hsout - not inverted

        self.i2c.write_lpGBT(0x021, 0x38)
        self.i2c.write_lpGBT(0x027, 0x55)
        self.i2c.write_lpGBT(0x028, 0x0f) # default is 0x5...?
        self.i2c.write_lpGBT(0x02f, 0xA) # max header found count = 10
        self.i2c.write_lpGBT(0x030, 0xA) # max header found count after nf
        self.i2c.write_lpGBT(0x031, 0xA) # max header not found count
        self.i2c.write_lpGBT(0x032, 0xA) # max skip cycle count after nf

        # PLL good
        self.i2c.write_lpGBT(0x0ef, 1 << 1)
        start = time.time()
        lastStatus = -1
        for i in range(20):
            time.sleep(0.05)
            status = self.i2c.read_lpGBT(0x1c7)
            print("{:.3f}s - Status is {}".format(time.time() - start, status))
            if lastStatus == status and status >= 13:
                break
            lastStatus = status
        if status < 13:
            raise Exception("lpGBT PUSM status has not reached PAUSE_FOR_DLL_CONFIG: {}".format(status))

    def configDLL(self):
        #    Set high-speed Uplink Driver current:
        self.write(0x039, 1 << 5) # bit 7: enable pre-emphasis, bits 0:6: high-speed line driver modulation current -> 32*137uA

        # DLL stuff
        self.write(0x01f,0x55) # from quickstart
        self.write(0x034, 1 << 6 | 1 << 4 | 1 << 2)
        self.write(0x033, 5 << 4 | 1 << 2 | 1)

        # PLL and DLL good
        self.write(0x0ef, 1 << 2 | 1 << 1)
        start = time.time()
        lastStatus = -1
        for i in range(20):
            time.sleep(0.05)
            status = self.getStatus()
            print("{:.3f}s - Status is {}".format(time.time() - start, status))
            if lastStatus == status and status == 18:
                break
            lastStatus = status
        if status != 18:
            raise Exception("lpGBT PUSM status is not ready: {}".format(status))


    def configLPGBT(self):
        """DEPRECATED - use configPLLI2C and configDLL - Minimally configure and start up lpGBT"""

        # addr = self.i2c.read_lpGBT(0x141)
        # print('Reading self.i2c address (register 0x141) : 0x%.2x'%(addr))

        #    Configure ClockGen Block:
        self.i2c.write_lpGBT(0x01f,0x55)
        self.i2c.write_lpGBT(0x020,0xc8) # X
        self.i2c.write_lpGBT(0x021,0x24)
        self.i2c.write_lpGBT(0x022,0x44) # 0 if TRX?
        self.i2c.write_lpGBT(0x023,0x55)
        self.i2c.write_lpGBT(0x024,0x55)
        self.i2c.write_lpGBT(0x025,0x55) # X
        self.i2c.write_lpGBT(0x026,0x55) # X
        self.i2c.write_lpGBT(0x027,0x55)
        self.i2c.write_lpGBT(0x028,0x0f) # default is 0x5...?
        self.i2c.write_lpGBT(0x029,0x00) # default is 0x1b...?
        self.i2c.write_lpGBT(0x02a,0x00)
        self.i2c.write_lpGBT(0x02b,0x00)
        self.i2c.write_lpGBT(0x02c,0x88)
        self.i2c.write_lpGBT(0x02d,0x89) # only TX, default: after SEU...?
        self.i2c.write_lpGBT(0x02e,0x99) # only TX, default: after SEU...?

        # datapath configuration
        self.i2c.write_lpGBT(0x132, 0x00) # make sure that all the elements are enabled, no data path bypass

        # correct polarity of the high speed datapath
        self.i2c.write_lpGBT(0x036, 1 << 6) # hsin - inverted, hsout - not inverted

        # Uplink:  ePort Inputs DLL's
        # 7:6 DLL charge pump = 4uA
        # 5:4 Number of clock cycles (in the 40 MHz clock domain) to confirm locked state = 4
        # 3 Force clock of ePortRx DLL state machine to be always enabled (disables clock gating). = 1
        # 2 Use coarse detector for the DLL lock condition. = 0
        # 1 Allow re-initialization in ePortRxGroup when the tuning is out of range. = 0
        # 0 Enable data gating. = 0
        # --> use quick start "default" + coarse detection + disable data gating because phase settings are inverted 2<->3
        self.i2c.write_lpGBT(0x034, 1 << 6 | 1 << 4 | 1 << 2)

        # recommendation from https://lpgbt.web.cern.ch/lpgbt/manual/quickStart.html#phase-shifter-clocks
        self.i2c.write_lpGBT(0x033, 5 << 4 | 1 << 2 | 1)

        #    Set high-speed Uplink Driver current:
        self.i2c.write_lpGBT(0x039, 1 << 5) # bit 7: enable pre-emphasis, bits 0:6: high-speed line driver modulation current -> 32*137uA

        #   Enable PowerGood @ .95V, Delay 100 ms:
        self.i2c.write_lpGBT(0x03e, 1 << 7 | 5 << 4 | 12)

        # now set frame aligner
        self.i2c.write_lpGBT(0x02f, 0xA) # max header found count = 10
        self.i2c.write_lpGBT(0x030, 0xA) # max header found count after nf
        self.i2c.write_lpGBT(0x031, 0xA) # max header not found count
        self.i2c.write_lpGBT(0x032, 0xA) # max skip cycle count after nf

        # Finally, Set pll (bit 1) & dll (bit 2) ConfigDone to run chip:
        self.i2c.write_lpGBT(0x0ef, 1 << 2 | 1 << 1)

        time.sleep(0.2)

        status = self.getStatus()
        if status != 18:
            raise Exception("lpGBT PUSM status is not ready: {}".format(status))

    def configDownLink(self, group, current=3, preemph=0, invert=False):
        """Enable and configure Tx link group (channel 0) at 160 Mbps

        Args:
            group (0-3): downlink group
            current (1-7, default 3): driving strength (1-7=1-4 mA in steps of .5 mA)
            preemph (0-7, default 0): pre-emphasis (1-7=1-4 mA in steps of .5 mA)
            invert (default True): invert polarity
        """
        assert(group >= 0 and group <= 3)
        assert(current >= 0 and current <= 7)
        assert(preemph >= 0 and preemph <= 7)

        # Downlink: ePort TX Groups at 160 Mbps (datarate = 2)
        currGroups = self.read(0x0a7)
        self.write(0x0a7, currGroups | (2 << 2 * group))

        # enable channel 0
        chanAddr = 0x0a9 if group < 2 else 0x0aa
        currChan = self.read(chanAddr)
        self.write(chanAddr, currChan | (1 << 4 * (group % 2)))

        TXConfig = 0
        if preemph > 0:
            TXConfig |= preemph << 5 | 3 << 3
        TXConfig |= current # 1-7 = 1-4 mA, steps of .5mA (0=0)
        self.write(0x0ac + 4 * group, TXConfig)

        if invert:
            # invert data on downlink
            self.write(0x0bc + 2 * group, 1 << 3)
        else:
            self.write(0x0bc + 2 * group, 0)

    def disableDownLink(self, group):
        """ Disable specific downlink group """

        assert(group >= 0 and group <= 3)
        currCfg = self.read(0x0a7)
        newCfg = currCfg & ~(0b11 << 2 * group)
        self.write(0x0a7, newCfg)

    def configUpLink(self, group, equal=0, phaseMode=0, phase=0, enableTerm=True, enableBias=True, invert=False):
        """ Config given elink Rx group, channel 0 for 1.28 Gbps (640 Mbps if running in 5G mode)

        Args:
            group (0-6): uplink group
            equal (0-3): equalization mode
            phaseMode (0-3):
                0 = constant
                1 = initial training (not implemented)
                2 = continous (not implemented)
                3 = continous with initial phase (not implemented)
            phase (0-14): sampling phase, used for phase mode = constant or continuous with initial
            invert: invert polarity
        """
        if phaseMode > 0:
            raise Exception("not implemented")
        assert(group >= 0 and group <= 6)
        assert(equal >= 0 and equal <= 3)
        assert(phase >= 0 and phase <= 14)

        # enable channel 0 in group, datarate = 3
        self.write(0x0c4 + group, 1 << 4 | 3 << 2 | phaseMode)

        RXConfig = 0
        RXConfig |= enableTerm << 1 # enable 100 ohm termination
        RXConfig |= enableBias << 2 # enable AC bias
        RXConfig |= (phase << 4) | (invert << 3) | (equal >> 1)
        self.write(0x0cc + 4 * group, RXConfig)
        # register contains EQ[0] setting of two groups -> make sure we don't change the other one
        currCfg = self.read(0x0e9 + group // 2)
        eqReg = (currCfg & ~(1 << (4 * (group % 2)))) | ((equal & 1) << (4 * (group % 2)))
        self.write(0x0e9 + group // 2, eqReg)

    def disableUpLink(self, group):
        """ Disable specific uplink group """
        assert(group >= 0 and group <= 6)
        self.write(0x0c4 + group, 0)

    def configBERT(self, group):
        """ Configure pattern checker to test for PRBS7 on given uplink group"""
        assert(group >= 0 and group <= 6)
        # BERT configuration for uplink data
        bertConfig = 0
        # coarse BERT source
        bertConfig |= (1 + group) << 4
        # check PRBS7 on channel 0 at data rate = 3 (1.28G)
        bertConfig |= 6
        self.write(0x126, bertConfig)

    def runBERT(self, testTime=0, verbose=False):
        """ Run BERT

        Args:
            testTime (0-15): number of bits checked -> 2^(5+2*n) * 32 bits at 1.28Gbps
            verbose: print test status bits and result

        Returns: bit error rate
        """
        self.write(0x127, testTime << 4 | 1) # also start measurement!
        bitsChecked = 2**(5 + 2*testTime) * 32

        status = 2
        while status & 2:
            time.sleep(.1)
            status = self.read(0x1bf)
            fsm = self.getStatus()
            if fsm != 18:
                print("Warning: lost FSM -> status = {}".format(fsm))
            if verbose:
                print("-- test status: {}, FSM: {}".format(status, fsm))

        if verbose:
            print("Finished with status {}".format(status))

        if status & (1 << 2):
            # stop measurement
            self.write(0x127, 0)
            raise Exception("BERT error flag (there was not data on the input)")

        bertResult = self.read(0x1c4) | self.read(0x1c3) << 8 | self.read(0x1c2) << 16 | self.read(0x1c1) << 24 | self.read(0x1c0) << 32

        # stop the measurement
        self.write(0x127, 0)

        # calculate Bit Error Rate
        ber = (bertResult + 1) / bitsChecked
        if verbose:
            print("BER: {} ({}/{})".format(ber, bertResult, bitsChecked))

        return ber

    def resetBERT(self):
        """Reset pattern checker register (run this if you stopped a test while it was running)"""
        self.write(0x127, 0)

    def __setPattern(self, pattern, baseAddr):
        """ pattern should be a 32 bit DC-balanced number """
        assert("{:0>32b}".format(pattern).count('0') == "{:0>32b}".format(pattern).count('1'))
        for i in range(0, 4):
            move = 8 * (3 - i)
            cPattern = ( pattern & (0xFF << move) ) >> move
            self.write(baseAddr + i, cPattern)

    def setDPPattern(self, pattern):
        """ Set constant pattern used in pattern generators (should be 32 bit DC-balanced number)"""
        self.__setPattern(pattern, 0x11e)

    def setBERTPattern(self, pattern):
        """ Set constant pattern used in pattern checker"""
        self.__setPattern(pattern, 0x128)

    def modeUpLink(self, source=0, pattern=0x77778888):
        """ Configure internal uplink data source for all groups

        Args:
            source (0-6):
                0: eport data
                1: PRBS7
                2: count up
                3: count down
                4: const pattern
                5: const pattern inverted
                6: loop back downlink frame
            pattern: constant pattern used for source = 4 or 5 (should be 32 bit DC-balanced number)
        """
        if source == 4 or source == 5:
            self.setDPPattern(pattern)

        # 0 data from serializer
        # 9 (10G) Loop back, downlink frame repeated 4 times
        # 10 (5G) Loop back, downlink frame repeated 2 times, each bit repeated 2 times
        # 12 const pattern (8 x DPDataPattern)
        self.write(0x118, 0)

        # internal data source for all Rx groups
        self.write(0x119, (0b00 << 6) | (source << 3) | (source << 0) ) ## data mode for serializer = 0 (data from elinks) and group 0
        self.write(0x11a, (source << 3) | (source << 0) ) # groups 3,2
        self.write(0x11b, (source << 3) | (source << 0) ) # groups 5,4
        self.write(0x11c, (source << 0)) # group 6

    def modeDownLink(self, groups=[3], source=0, pattern=0x77778888):
        """ Configure Tx elink data source

        Args:
            groups (list of ints, 0-3): downlink groups
            source (0-3):
                0: downlink data
                1: PRBS7
                2: count up
                3: const pattern
            pattern: constant pattern used for source = 3 (should be 32 bit DC-balanced number)
        """
        if source == 3:
            self.setDPPattern(pattern)

        currVal = self.read(0x11d)
        for gr in groups:
            assert(gr >= 0 and gr <= 3)
            shift = 2 * gr
            currVal = (currVal & ~(0b11 << shift)) | (source << shift)
        self.write(0x11d, currVal)

    def findPhase(self, group, time=8, maxPhase=7, verbose=False):
        """ Find RX phase with lowest BER and set it

        Args:
            group: uplink ground to tune
            time: length of test (see BERT arguments)
            maxPhase: only scan up to that phase (default: only scan one eye out of two)
        Returns: (best phase, best BER)
        """
        assert(maxPhase >= 0 and maxPhase < 15)
        assert(time >= 0 and time <= 15)
        assert(group >= 0 and group <= 6)

        bitsChecked = 2**(5 + 2*time) * 32
        berts = []
        phases = list(range(0, maxPhase + 1))
        # FIXME
        phases[2] = 3
        phases[3] = 2

        print("Finding best phase for group {}".format(group))

        def chPh(gr, ph):
            # make sure we only change the phase
            RXConfig = self.read(0x0cc + 4 * gr)
            RXConfig = (RXConfig & ~(0b1111 << 4)) | ph << 4
            self.write(0x0cc + 4 * gr, RXConfig)

        self.configBERT(group)
        for ph in phases:
            chPh(group, ph)
            berts.append(self.runBERT(time, verbose))

        # Find middle of first contiguous region with lowest BER
        bestBER = min(berts)
        if bestBER > 2. / bitsChecked:
            print("Warning: best BER is {} - something is probably wrong.".format(bestBER))

        # first start from below
        minPhIdxLow = berts.index(bestBER)
        maxPhIdxLow = minPhIdxLow
        for i in range(minPhIdxLow, len(berts) - 1):
            if berts[i+1] == bestBER:
                maxPhIdxLow += 1
            else:
                break
        # also try from above
        berts.reverse()
        minPhIdxHigh = berts.index(bestBER)
        maxPhIdxHigh = minPhIdxHigh
        for i in range(minPhIdxHigh, len(berts) - 1):
            if berts[i+1] == bestBER:
                maxPhIdxHigh += 1
            else:
                break
        berts.reverse()

        # take the middle of the largest range
        if (maxPhIdxLow - minPhIdxLow) > (maxPhIdxHigh - minPhIdxHigh):
            bestPhIdx = (minPhIdxLow + maxPhIdxLow) // 2
            bestPh = phases[bestPhIdx]
        else:
            bestPhIdx = (minPhIdxHigh + maxPhIdxHigh) // 2 + 1
            bestPh = phases[len(berts) - bestPhIdx - 1]

        if verbose:
            print("Best phase is {} with BER = {}. Setting it.".format(bestPh, bestBER))
        chPh(group, bestPh)

        return bestPh, bestBER
